public class DuplicateZeros {
    public static void main(String[] args) {
        int [] arr = {1,0,2,3,0,4,5,0};
        int len = arr.length;
        int numZ = 0;

        for (int i=0; i<len; i++){
            if(arr[i]== 0){
                numZ++;
            }
        }

        for (int i = len-1 , j = len+numZ-1 ; i >= 0; i--, j--) {
            if (arr[i] == 0) {
                // Duplicate zero
                if (j < len) {
                    arr[j] = 0;
                }
                j--;
            }
            if (j < len) {
                arr[j] = arr[i];
            }
        }

        for(int i=0; i<len; i++){
            System.out.print(arr[i]+" ");
        }
    }
}
