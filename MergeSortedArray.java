public class MergeSortedArray {
    public static void main(String[] args) {
        int num1 [] = {1,2,3,0,0,0};
        int num2 [] = {2,5,6};
        int m = 3;
        int n = 3;
        mergeS(num1, num2, m, n);
        printArr(num1);
}
    private static void printArr(int num1[]) {
        for(int i = 0; i < num1.length; i++){
            System.out.print(num1[i]+" ");
        }
    }

    static int [] mergeS(int num1[], int num2[],int m ,int n){
        int min;
        int len = m+n-1;
        int M = m - 1;
        int N = n - 1;
        while (N >= 0) {
            if (M > 0 && num1[M] > num2[N]) {
                num1[len] = num1[M];
                M--;
            } else {
                num1[len] = num2[N];
                N--;
            }
            len--;
        }

        //Sorted
        for(int i=0; i<num1.length; i++){
            for(int j=0; j<num1.length; j++){
                if(num1[i] < num1[j]){
                    min = num1[i];
                    num1[i] = num1[j];
                    num1[j] = min;  
                }
            }
        }
        return num1;
    }
}