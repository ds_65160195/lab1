public class ArrayManipulation {
    public static void main(String[] args) {
        //An integer array named "numbers" with the values {5, 8, 3, 2, 7}.
        //A string array named "names" with the values {"Alice", "Bob", "Charlie", "David"}.
        //A double array named "values" with a length of 4 (do not initialize the elements yet).
        int [] number = {5,8,3,2,7};
        String [] names = {"Alice","Bob","Charlie","David"};
        double [] values = new double[4];
        String [] reversedNames = new String[4];
        int sum = 0;
        double max = 0.0;
        int min;
        //Print the elements of the "numbers" array using a for loop.
        for(int i = 0; i < number.length; i++){
            System.out.print(number[i]+" ");
        }
        System.out.println();
        //Print the elements of the "names" array using a for-each loop.
        for(int i=0; i<names.length; i++){
            System.out.print(names[i]+" ");
        }
        System.out.println();
        //Initialize the elements of the "values" array with any four decimal values of your choice.
        for(int i=0; i<4; i++){
            values[i] = i+1.1;
        }
        //Calculate and print the sum of all elements in the "numbers" array.
        for(int i=0; i<number.length; i++){
            sum = number[i]+sum;
        }
        System.out.println(sum);
        //Find and print the maximum value in the "values" array.
        for(int i=0; i<values.length; i++){
            if(values[i] > max){
                max = values[i];
            }
        }
        System.out.println(max);
        //Create a new string array named "reversedNames" with the same length as the "names" array.
        //Fill the "reversedNames" array with the elements of the "names" array in reverse order. Print the elements of the "reversedNames" array.
        for(int i=0; i<4; i++){
            reversedNames[i] = names[3-i];
            System.out.print(reversedNames[i]+" ");
        }
        System.out.println();
        //Bonus
        for(int i=0; i < number.length; i++){
            for(int j=0; j<number.length; j++){
                if(number[i] < number[j]){
                    min = number[i];  
                    number[i] = number[j];  
                    number[j] = min;  
                }
            }
        }
        for(int i = 0; i < number.length; i++){
            System.out.print(number[i]+" ");
        }
    }
}